var express = require('express')
var router = express.Router()
const jwt = require('jsonwebtoken');
var services = require('../services')

var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);

var environment = require('../config/environment')

router.get('/', async function (req, res, next) {
    let users = await services.user_service.getAll();
    res.status(200).json(users);
});

router.get('/:id', function (req, res, next) {
    // Find by id (request.params.id), delegate to service
});

router.get('/exists/:id', function (req, res, next) {
    // Exists by id (request.params.id), delegate to service
});

router.get('/count', function (req, res, next) {
    // Get entity count, delegate to service
});

router.delete('/:id', function (req, res, next) {
    // Delete by id (request.params.id), delegate to service
});

router.post('/', function (req, res, next) {
    // Create new, delegate to service
});

router.put('/', function (req, res, next) {
    // Update existing, delegate to service
    var { username, level } = req.body;

    let result = services.user_service.update({username, level})
    res.status(200).json({message: 'ok'})


});


router.post('/login', async function (req, res, next) {
    // {'username': 'string', 'password': 'string'}

    let { username, password } = req.body;
    let user = await services.user_service.findByUsername(username);

    let valid = await bcrypt.compare(password, user.password);
    if (!valid)
        throw new Error('Invalid password');

    delete user.password;
    user.roles = [(await services.user_service.getRolesForUser(user.id)).role_name];

    const token = jwt.sign(user, environment.secret_key);

    res.status(200).json({ token });
});


router.post('/register', async function (req, res, next) {
    // {'username': 'string', 'password': 'string', 'first_name': 'string', 'last_name': 'string', 'role': 'string (enum)'}

    var { username, password, email, role } = req.body;
    
    var hash = bcrypt.hashSync(password, salt);

    // Store hash as user password in db (delegate to service)

    let result = services.user_service.create({username, password: hash, email, role})

    res.status(200).json({message: 'ok'})
    

});

module.exports = router