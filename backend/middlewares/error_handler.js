var handler = (err, req, res, next) => {
    res.status(500)
    res.send(err.toString())
}

module.exports = handler;