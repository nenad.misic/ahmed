const jwt = require('jsonwebtoken');

var handler = (req, res, next) => {
    // const authHeader = req.headers['authorization'];
    // const token = authHeader && authHeader.split(' ')[1] // checks if it's undefined; [1]: Bearer + token

    // if (token == null)
    //     return res.status(401).send('Please, sign in');

    // jwt.verify(token, process.env.SECRET, (err, user) => {
    //     if (err)
    //         return res.status(403).send('Access denied :)');

    //     req.user = user;
    //     next();
    // })

    if (req.headers.authorization) req.logged_user = jwt.decode(req.headers.authorization.split(' ')[1]);
    console.log("TODO - Authentication (method called)");
    next();
}

module.exports = handler;