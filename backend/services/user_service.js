const { user_repository } = require('../repositories');
var repositories = require('../repositories');

var service = {
    create: async (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        let uid = (await repositories.user_repository.create(entity)).lastID;
        let rid = (await repositories.user_repository.getRoleByName(entity.role)).id
        let result = await repositories.user_repository.addRoleToUser(uid, rid)
        return result;
    },
    update: async (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        return (await repositories.user_repository.update(entity));
    },
    delete: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
    },
    exists: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
    },
    count: () => {
        // Delegates to repository.
    },
    findById: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
    },
    findByUsername:  (username) => {
        return repositories.user_repository.findByUsername(username);
    },
    getAll: () => {
        return repositories.user_repository.getAll();
    },
    getRolesForUser: (id) => {
        return repositories.user_repository.getRolesForUser(id);
    }
}

module.exports = service;

