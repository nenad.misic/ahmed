var sqlite3 = require('sqlite3');
var open = require('sqlite').open;

var getDbConnection = () => {
    return open({
        filename: '../database.db',
        driver: sqlite3.Database
    });
}
 
module.exports = {
    getDbConnection,
}