var express = require('express');
var path = require('path');
var logger = require('morgan');
var http = require('http');
const cors = require('cors');

var routes =  require('./routes');
var middlewares = require('./middlewares')

var app = express();


app.use(cors({ origin: 'http://localhost:3000' }));
app.unsubscribe(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(middlewares.auth_handler);

app.use(express.static(path.join(__dirname, 'public')));

app.use('/user', routes.user_routes);
// app.use('/project', routes.project_routes);

app.use(middlewares.error_handler);

app.get('/', (req, res, next) => {
    res.send('Success');
});

var server = http.createServer(app);
server.listen(process.env.PORT || 1337);
console.log(`Listening on port ${process.env.PORT || 1337}`);

module.exports = app;