
-- INSERT INTO user (first_name, last_name, username, password) VALUES ("DummyName1", "DummyLastName1", "username1", "$2a$10$lQS4BxhQlNMUniNZZdomI.oMeod1Zn8ez9ORTsHe5vWe1Z46OQ8Se");
-- INSERT INTO user (first_name, last_name, username, password) VALUES ("DummyName2", "DummyLastName2", "username2", "$2a$10$lQS4BxhQlNMUniNZZdomI.oMeod1Zn8ez9ORTsHe5vWe1Z46OQ8Se");
-- INSERT INTO user (first_name, last_name, username, password) VALUES ("DummyName3", "DummyLastName3", "username3", "$2a$10$lQS4BxhQlNMUniNZZdomI.ufOw4VE41I4p2U61QGxQJGDVl.IE1kS");
-- INSERT INTO user (first_name, last_name, username, password) VALUES ("DummyName4", "DummyLastName4", "username4", "$2a$10$lQS4BxhQlNMUniNZZdomI.ufOw4VE41I4p2U61QGxQJGDVl.IE1kS");

-- INSERT INTO role (role_name) VALUES ("professor");
-- INSERT INTO role (role_name) VALUES ("student");

-- INSERT INTO user_role (u_id, r_id) VALUES (1,2);
-- INSERT INTO user_role (u_id, r_id) VALUES (2,2);
-- INSERT INTO user_role (u_id, r_id) VALUES (3,1);
-- INSERT INTO user_role (u_id, r_id) VALUES (4,1);

INSERT INTO role (role_name) VALUES ("worker");
INSERT INTO role (role_name) VALUES ("client");
INSERT INTO role (role_name) VALUES ("admin");